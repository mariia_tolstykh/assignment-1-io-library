%define    WRITE   1
%define    READ    0
%define    STDOUT  1
%define    STDIN   0
%define SYSCALL_EXIT 60


section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
    mov rax, SYSCALL_EXIT  ; exit syscall number или номер системного вызова exit
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax  ;0 в счетчик
.loop:
    cmp byte[rdi+rax], 0  
    je .end  ;переход, если сумма 0
    inc rax
    jmp .loop  ;повторение цикла
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout .
print_string:
    push rdi                
    call string_length      
    mov rdx, rax            ; длина строки в rdx
    pop rsi                
    mov rax, STDOUT         ; номер системного вызова
    mov rdi, STDOUT        ; дескриптор stdout
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA  
    ; Принимает код символа и выводит его в stdout .
    print_char:
    xor rax, rax
    mov rax, WRITE
    push rdi
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате .
print_int:
    xor rax, rax
    mov rax, rdi ; перенесем аргумент в аккумулятор
    test rax, rax ; проставим флаги
    jns .plus ; если положительное число, то выводим через print_uint
    mov rdi, '-' ; иначе пишем минус
    push rax ; rax изменится, поэтому сохраним его
    call print_char
    pop rax ; восстановим
    neg rax ; приведем к положительному числу
    mov rdi, rax
    .plus:
    ; Выводит беззнаковое 8-байтовое число в десятичном формате 
    ; Совет: выделите место в стеке и храните там результаты деления
    ; Не забудьте перевести цифры в их ASCII коды.
    print_uint:
    mov r9, 10          ;число 10 - основа 10-ной сс
    mov rax, rdi        ;число в аккум
    push 00h            ;0 в стек, для обозначения конца строки
    
    .loop:
        xor rdx, rdx    ;обнуляем предыдущий остаток
        div r9          ;делим аккум на 10, результат в rax, остаток в rdx
        add dx, 030h    ;переводим в ASCII (030h = '0' в ASCII)
        push rdx        ;остаток в стек, там и собирается все наше число  
        cmp rax, 0      ;проверка оставшегося числа на 0
        jne .loop       ;если не 0, то продолжаем делить

    .print:
        pop rax         ;достаем элемент из стека
        cmp al, 0       ;проверка на конец строки
        je .ret

        mov rdi, rax    ;цифру в rdi для вызова функции
        call print_char ;вызываем вывод символа
        jmp .print      ;переходим к выводу след символа

    .ret:
        ret
    

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax				; счетчик
    .loop:
	mov dl, byte [rdi + rax]
	cmp byte [rsi + rax], dl		
	jne .no					
	cmp byte [rsi + rax], 0			; проверка на конец строки
	je .yes
	inc rax					; увеличиваем счетчик
	jmp .loop
    .yes:
	mov rax, 1   ; возвращаем 1
	ret
    .no:
	xor rax, rax ; возвращаем 0
	ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax    ; 0 в rax
    push rax	    
    mov rsi, rsp    ; адрес символа
    mov rdx, 1	    ; длина
    mov rdi, STDIN
    syscall
    pop rax	    
    ret
 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;rdi-буфер, rsi-размер буфера
read_word:
        push r12
        push r13
        push r14
        mov r12, rdi    ;адрес буфера
        test rsi, rsi   ;проверка на пустой буфер
        jz .stop
        mov r13, rsi    ;размер буфера

    .space:
        call read_char      ;символ в rax
        cmp al, ' '       ;проверка на пробел
        je .space
        cmp al, `\t`       ;проверка табуляции
        je .space
        cmp al, `\n`       ;проверка перевода строки
        je .space

        xor r14, r14       ;0 в счетчик кол-ва символов
    .symbols:
        test rax, rax          ;проверка на конец строки
        jz .end
        cmp r14, r13        ;проверяем, не вышли ли за буфер
        jae .long
        mov byte[r12+r14], al   
        inc r14             
        call read_char      
        cmp al, ' '       ;проверка на пробел
        je .end
        cmp al, `\t`       ;проверка табуляции
        je .end
        cmp al, `\n`        ;проверка перевода строки
        je .end
        jmp .symbols        ;след символ
        .long:
            xor rax, rax        ;0 если слишком большое слово
            jmp .ret
    .end:
        mov byte[r12+r14], 0    ;конец строки
    .stop:    
        mov rdx, r14        ;кол-во символов
        mov rax, r12        ;адрес буфера
    .ret:
        pop r14
        pop r13
        pop r12
        ret


 ; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    xor rax, rax    ;0 в аккум 
    xor rdx, rdx    ;0 в счетчик длины
    xor rcx, rcx    ;0 в регистр(временное хранение символа)
    mov r8, 10
    
    .loop:
        mov cl, byte[rdi]   ;1 символ
        sub rcx, 030h       ;перевод из ASCII (030h = '0' в ASCII)
                        ;проверка на цифру
        cmp rcx, 9
        jg .ret         ;если > 9, то не число
        cmp rcx, 0
        jl .ret         ;если < 0, то не число

        push rdx
        mul r8          ;10*аккум(значение с прошлого шага)
        pop rdx

        add rax, rcx    ;прибавляем к числу новую цифру
        inc rdi         ;увеличиваем указатель на след символ
        inc rdx         ;увеличиваем счетчик длины
        jmp .loop       ;след символ
    .ret:
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'    ; сравниваем со знаком минус
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .ret  ; смотрим длину знака минуса и переделываем в доп код
    inc rdx
    neg rax
    .ret:
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
;rdi-строка, rsi-буфер, rdx-длина буфера
string_copy:
    push rdi                ;сохр для корректности
    push rsi
    call string_length      
    pop rsi
    pop rdi
    cmp rax, rdx            ;проверка входит ли строка в буфер
    jg .long                

    xor rax, rax            ;обнуляет сдвиг
    .loop:
        cmp rax, rdx        ;проверка не ушли ли за длину буфера
        jg .stop

        mov cl, byte[rdi+rax]   ;символ из строки в регистр
        mov byte[rsi+rax], cl   ;символ из регистра в буффер

        inc rax             ;увеличиваем сдвиг
        jmp .loop           ;след символ

    .stop:
        dec rax             ;уменьшаем кол-во символов(поскольку оно увеличивается до проверки)
        jmp .ret
    .long:
        xor rax, rax        ;0 если не входит в буфер
    .ret:  
        ret
